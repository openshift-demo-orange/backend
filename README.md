# backend

## app origin

https://codeburst.io/this-is-how-easy-it-is-to-create-a-rest-api-8a25122ab1f3

### local DEBUG


download  app.py :  https://gist.github.com/leon-sleepinglion/97bfd34132394e23ca5905ec730f776a


$ sudo apt install python-pip

$ pip install flask-restful


$  python app.py
* Restarting with stat
* Debugger is active!
* Debugger PIN: XXX-XXX-XXX
* Running on http://0.0.0.0:8080/


GET http://0.0.0.0:8080//user/Jass 
=> return a JSON Object

## deployment

Please read this before : https://blog.openshift.com/getting-started-python/

### deploy on OpenShift with Template

```
oc new-app https://gitlab.com/openshift-demo-orange/backend.git --name=app-demo-backend
oc expose svc/app-demo-backend
```
